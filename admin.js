function getList(model, models, sort, range, filter) {
  const Op = models.sequelize.Op;
  const queryOpts = {};

  if (range) {
    const [rangeFrom, rangeTo] = range;
    queryOpts.offset = rangeFrom;
    queryOpts.limit = rangeTo - rangeFrom + 1;
  }

  if (sort) {
    queryOpts.order = [sort];
  }

  if (filter) {
    const where = {};

    for (const [field, value] of Object.entries(filter)) {
      if (field == 'ids') {
        where.id = { [Op.in]: value };
      } else {
        where[field] = value;
      }
    }

    queryOpts.where = where;
  }

  // TODO: embed parameter

  return model.findAndCountAll(queryOpts);
}

function getOne(model, models, id) {
  return model.findById(id);
}

function create(model, models, data) {
  return model.create(data);
}

function update(model, models, id, data) {
  return model.update(data, { where: { id } })
    .then(updated => {
      if (updated) {
        return model.findById(id);
      }

      return {};
    });
}

async function remove(model, models, id, data) {
  const object = await model.findById(id);
  await object.destroy();
  return object;
}

module.exports = {
  getList,
  getOne,
  create,
  update,
  remove
};

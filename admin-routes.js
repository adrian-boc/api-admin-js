const express = require('express');
const apiCommon = require('api-common');

const admin = require('./admin');
const response = apiCommon.response;

function adminRoutes(model, resource, db, options) {
  const router = express.Router();

  const { authCheck } = options;

  // TODO: auth
  router.get('/', authCheck, (req, res) => {
    let sort = req.query.sort;

    if (sort) {
      try {
        sort = JSON.parse(sort);
      } catch (e) {
        sort = null;
      }
    }

    let range = req.query.range;

    if (range) {
      try {
        range = JSON.parse(range);
      } catch (e) {
        range = null;
      }
    }

    let filter = req.query.filter;

    if (filter) {
      try {
        filter = JSON.parse(filter);
      } catch (e) {
        filter = null;
      }
    }

    admin.getList(model, db, sort, range, filter)
      .then(result => {
        if (range) {
          const [rangeFrom, rangeTo] = range;
          res.set('Content-Range', `${resource} ${rangeFrom}-${rangeTo}/${result.count}`);
        }
        response.success(res, result.rows);
      });
  });

  router.get('/:objId', authCheck, (req, res) => {
    admin.getOne(model, db, req.params.objId)
      .then(result => {
        response.success(res, result);
      });
  });

  router.post('/', authCheck, (req, res) => {
    const data = req.body;

    let dataValidation = Promise.resolve(data);
    if (options && options.createSchema) {
      dataValidation = Joi.validate(data, options.createSchema)
    }

    dataValidation.then(data => admin.create(model, db, data))
      .then(result => {
        response.success(res, result);
      })
      .catch(err => {
        res.end();
      });
  });

  router.put('/:objId', authCheck, (req, res) => {
    admin.update(model, db, req.params.objId, req.body)
      .then(result => {
        response.success(res, result);
      });
  });

  router.delete('/:objId', authCheck, (req, res) => {
    admin.remove(model, db, req.params.objId)
      .then(result => {
        response.success(res, result);
      });
  });

  return router;
}

module.exports = adminRoutes;
